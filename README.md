#README#
**Programming Test - Level 3 Software Developer Evaluation**

Author: James Dahms

Email: jamesdahms@gmail.com

Company: Transmax

Appveyor: [Programing Test Level 3 - grade-scores](https://ci.appveyor.com/project/jamesd/programming-test-lvl3)

---

**Program: grade-students**

grade-scores is a console app that:

- Takes as a parameter a string that represents a text file containing a list of names, and their scores
- Orders the names by their score. If scores are the same, order by their last name followed by first name
- Creates a new text file called <input-file- name>-graded.txt with the list of sorted score and names.
- Takes a CSV input file with the format of First Name, Surname, Score
- Output file format is CSV with the format of Surname, First Name, Score

---

**Example**

Example, if the input file (students.txt) contains:

TED, BUNDY, 88

ALLAN, SMITH , 85

MADISON , KING, 83

FRANCIS, SMITH, 85

Then the output file (students-graded.txt) would be:

BUNDY, TED, 88

SMITH, ALLAN, 85

SMITH, FRANCIS, 85

KING, MADISON, 83