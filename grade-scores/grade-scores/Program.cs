﻿namespace grade_scores
{
    class Program
    {
        static void Main(string[] args)
        {
            var validator = new Validator();
            var fileProcessor = new FileProcessor();

            if (validator.ValidateArgs(args))
            {
                fileProcessor.ProcessFile(validator.TextFile);
            }
        }
    }
}
