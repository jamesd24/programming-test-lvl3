﻿using System.IO;
using System.Linq;
using grade_scores.Properties;

namespace grade_scores
{
    public class Validator
    {
        public string TextFile;

        /// <summary>
        /// Validates the input arguments from the command line.
        /// Should only have one argument as a txt file.
        /// </summary>
        /// <param name="args"></param>
        /// <returns>Validation results</returns>
        public bool ValidateArgs(string[] args)
        {
            if (args.Length > 1 || !args.Any() || args.First().Trim() == "?")
            {
                Util.PrintUsage();
                return false;
            }

            return ValidateFile(args.First().Trim());
        }

        private bool ValidateFile(string file)
        {
            if (!File.Exists(file))
            {
                Util.PrintError(string.Format(Resources.FILE_NOT_FOUND_ERROR, file));
                return false;
            }

            if (!IsTxt(file))
            {
                Util.PrintError(string.Format(Resources.FILE_TYPE_ERROR, file));
                return false;
            }

            TextFile = file;
            return true;
        }

        private bool IsTxt(string file)
        {
            return file != null && file.EndsWith(".txt");
        }
    }
}