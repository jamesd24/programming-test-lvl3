﻿using System;
using grade_scores.Properties;

namespace grade_scores
{
    public static class Util
    {
        /// <summary>
        /// Prints string to console stdout. 
        /// Only if app is compiled in DEBUG
        /// </summary>
        /// <param name="str"></param>
        public static void Print(string str)
        {
#if DEBUG
            Console.WriteLine(str);
#endif
        }

        /// <summary>
        /// Prints an array of strings to console stdout as separate lines.
        /// Only if app is compiled in DEBUG
        /// </summary>
        /// <param name="str"></param>
        public static void Print(string[] str)
        {
            foreach (string s in str)
            {
                Print(s);
            }
        }

        /// <summary>
        /// Prints string with error outlining.
        /// Only if app is compiled in DEBUG
        /// </summary>
        /// <param name="err"></param>
        public static void PrintError(string err)
        {
            Print($"ERROR: {err}");
        }

        /// <summary>
        /// Prints the usage of grade-scores.exe
        /// </summary>
        public static void PrintUsage()
        {
            Console.WriteLine(Resources.USAGE);
        }
    }
}
