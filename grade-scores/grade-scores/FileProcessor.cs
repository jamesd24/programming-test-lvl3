﻿using System;
using System.IO;
using System.Linq;

namespace grade_scores
{
    public class FileProcessor
    {        
        /// <summary>
        /// Takes an input string representing a path to a file containing a list of names and scores
        /// then writes the list of names and scores ordered by score then by last and first name in a separate file (-graded.txt)
        /// </summary>
        /// <param name="file"></param>
        public bool ProcessFile(string file)
        {
            try
            {
                WriteStudents(GetGradedStudents(File.ReadAllLines(file)), file);
                return true;
            }
            catch (Exception ex)
            {
                Util.PrintError(ex.Message);
                return false;
            }
        }

        protected string[] GetGradedStudents(string[] input)
        {
            var students = input.Select(s => new Student(s));
            return students.OrderByDescending(g => g.Score).ThenBy(g => g.Surname).ThenBy(g => g.FirstName).Select(g => g.ToCsv()).ToArray();
        }

        private void WriteStudents(string[] students, string file)
        {
            string gradedFile = file.Replace(".txt", "-graded.txt");
            Util.Print(students);
            File.WriteAllLines(gradedFile, students);
            Util.Print($"Created {gradedFile}");
        }
    }
}
