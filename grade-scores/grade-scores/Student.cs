﻿using System;
using grade_scores.Properties;

namespace grade_scores
{
    public class Student
    {
        public string FirstName;
        public string Surname;
        public int Score;

        public Student(string str)
        {
            ParseInputCsv(str.Split(','));
        }

        public string ToCsv()
        {
            return $"{Surname}, {FirstName}, {Score}";
        }

        private void ParseInputCsv(string[] str)
        {
            if (str.Length != 3)
            {
                throw new ArgumentException(Resources.INVALID_CSV_FORMAT_ERROR);
            }

            FirstName = str[0].Trim();
            Surname = str[1].Trim();
            var result = int.TryParse(str[2].Trim(), out Score);

            if (!result)
            {
                throw new ArgumentException(string.Format(Resources.SCORE_NOT_A_NUMBER_ERROR,FirstName,Surname));
            }
        }
    }
}
