﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace grade_scores.test
{
    [TestClass]
    public class GradeTests
    {
        private class Grader : FileProcessor
        {
            public string[] Grade(string[] input)
            {
                return GetGradedStudents(input);
            }
        }

        private void EvaluateSortedGrades(string[] expectedGrades, string[] students)
        {
            var result = new Grader().Grade(students);
            if (expectedGrades == null)
            {
                Assert.IsTrue(result.Length == 0);
            }
            else
            {
                Assert.IsTrue(expectedGrades.SequenceEqual(result));
            }
        }

        [TestMethod, TestCategory("Grade")]
        public void SortTest_LastFirstScoreSorted()
        {
            string[] students =
            {
                "TED, BUNDY, 88",
                "ALLAN, SMITH, 85",
                "MADISON, KING, 83",
                "FRANCIS, SMITH, 85"
            };

            var grades = students.Select(name => new Student(name).ToCsv()).ToArray();

            string[] expectedGrades =
            {
                grades[0],
                grades[1],
                grades[3],
                grades[2]
            };

            EvaluateSortedGrades(expectedGrades,students);
        }

        [TestMethod, TestCategory("Grade")]
        public void SortTest_LastFirstScoreSorted2()
        {
            string[] students =
            {
                "ALLAN, SMITH, 85",
                "TED, BUNDY, 88",
                "MADISON, KING, 83",
                "TEST, PERSON, 60",
                "FRANCIS, SMITH, 85"

            };

            var grades = students.Select(name => new Student(name).ToCsv()).ToArray();

            string[] expectedGrades =
            {
                grades[1],
                grades[0],
                grades[4],
                grades[2],
                grades[3]
            };

            EvaluateSortedGrades(expectedGrades, students);
        }

        [TestMethod, TestCategory("Grade")]
        [ExpectedException(typeof(ArgumentException))]
        public void SortTest_ScoreNotANumber()
        {
            string[] students =
            {
                "TED, BUNDY, 88",
                "ALLAN, SMITH, 85",
                "MADISON, KING, 83f",
                "FRANCIS, SMITH, 85"
            };

            EvaluateSortedGrades(null, students);
        }

        [TestMethod, TestCategory("Grade")]
        [ExpectedException(typeof(ArgumentException))]
        public void SortTest_ScoreMissing()
        {
            string[] students =
            {
                "TED, BUNDY, 88",
                "ALLAN, SMITH",
                "MADISON, KING, 83",
                "FRANCIS, SMITH, 85"
            };

            EvaluateSortedGrades(null, students);
        }
    }
}

