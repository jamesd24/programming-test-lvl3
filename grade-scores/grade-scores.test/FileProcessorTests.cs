﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace grade_scores.test
{
    [TestClass]
    public class FileProcessorTests
    {
        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_SortedFileWritten()
        {
            var processor = new FileProcessor();
            Assert.IsTrue(processor.ProcessFile("students.txt"));
            Assert.IsTrue(File.Exists("students-graded.txt"));
        }

        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_WriteFileIOError()
        {
            var processor = new FileProcessor();
            //Create Names Sorted txt file
            Assert.IsTrue(processor.ProcessFile("students.txt"));
            //Lock output file
            var f = File.Open("students-graded.txt", FileMode.Open, FileAccess.Write, FileShare.None);
            //Try to write over a locked output file
            Assert.IsFalse(processor.ProcessFile("students.txt"));
            //Close file
            f.Close();
        }

        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_ReadFileIOError()
        {
            var processor = new FileProcessor();
            //Open file with read lock
            var f = File.Open("students.txt", FileMode.Open, FileAccess.Read, FileShare.None);
            //Try to read the locked input file
            Assert.IsFalse(processor.ProcessFile("students.txt"));
            //Close file
            f.Close();
        }

        [TestMethod, TestCategory("FileProcessor")]
        public void FileTest_ReadFileDeleted()
        {
            var processor = new FileProcessor();
            //The input text file could possibly be deleted between validate file and process file.
            string file = "deleted_file.txt";
            Assert.IsFalse(File.Exists(file));
            Assert.IsFalse(processor.ProcessFile(file));
        }
    }
}
