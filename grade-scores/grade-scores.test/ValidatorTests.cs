﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace grade_scores.test
{
    [TestClass]
    public class ValidatorTests
    {
        //Validate Arguments Tests
        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateNoArguments()
        {
            var v = new Validator();
            string[] args = {};
            Assert.IsFalse(v.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateMultipleArguments()
        {
            var v = new Validator();
            string[] args =
            {
                "test.txt",
                "test2.txt",
                "test3.txt"
            };

            Assert.IsFalse(v.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateOneArgument()
        {
            var v = new Validator();
            string[] args = { "students.txt" };
            Assert.IsTrue(v.ValidateArgs(args));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateHelpArgument()
        {
            var v = new Validator();
            string[] args = { "?" };
            Assert.IsFalse(v.ValidateArgs(args));
        }

        //Validate File Tests
        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateTxtFile()
        {
            var v = new Validator();
            string[] args = { "students.txt" };
            Assert.IsTrue(v.ValidateArgs(args));
            Assert.IsTrue(v.TextFile.Equals(args.First()));
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateFileNotFound()
        {
            var v = new Validator();
            string[] args = { "filenotfound.txt" };
            Assert.IsFalse(v.ValidateArgs(args));
            Assert.IsNull(v.TextFile);
        }

        [TestMethod, TestCategory("Validator")]
        public void ValidatorTest_ValidateFileNotTxt()
        {
            var v = new Validator();
            string[] args = { "students.nottxt" };
            Assert.IsFalse(v.ValidateArgs(args));
            Assert.IsNull(v.TextFile);
        }
    }
}

